# My Dots
## About
These are my dotfiles for my current, and pasts setups.

## My Current Setup
Currently, I am running [EndeavourOS Cassini](https://endeavouros.com/latest-release/), with [Hyprland](https://hyprland.org/).

![My current setup of EndeavourOS Cassini, running Hyprland](assets/01-23-current-setup.png)

__Things I Use__
- [Brave](https://brave.com) - Web Browser
- [Thunar](https://gitlab.xfce.org/xfce/thunar) - GUI File Manager
- [Kitty](https://github.com/kovidgoyal/kitty) - Terminal Emulator
- [Ranger](https://github.com/ranger/ranger) Terminal File Manager
- [Hyprland](https://hyprland.org/) - Tiling Window Manager
- [Waybar](https://github.com/Alexays/Waybar) - Top Bar
- [Swaylock](https://github.com/swaywm/swaylock) - Lock screen
- [Neovim](https://neovim.io/) - Terminal Text Editor

## How To Get
**To install this, you *MUST* have Arch or an Arch based distro installed.**

Add Chaotic-AUR:
```bash
$ pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
$ pacman-key --lsign-key 3056513887B78AEB
$ pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
```

Then install all the packages needed:
```bash
$ yay -S hyprland-bin polkit-gnome ffmpeg neovim viewnior dunst rofi pavucontrol thunar starship wl-clipboard wf-recorder swaybg grimblast-git ffmpegthumbnailer tumbler playerctl noise-suppression-for-voice thunar-archive-plugin kitty waybar-hyprland wlogout swaylock-effects sddm-git nwg-look-bin nordic-theme papirus-icon-theme pamixer brave
```

Then move all the files from `dotconfig/` into `~/.config/`.


[Based on Chris Titus Tech's Hyprland Setup](https://github.com/ChrisTitusTech/hyprland-titus)
